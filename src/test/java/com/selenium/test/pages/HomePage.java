package com.selenium.test.pages;

import com.selenium.framework.base.SeleniumBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("cucumber-glue")
public class HomePage extends SeleniumBase {

    @FindBy(xpath = "/html/body/div/table/tbody/tr/td[1]/table/tbody/tr/td/table/tbody/tr/td/p[2]/img")
    public static WebElement HOMEPAGETITLE;
  

    public HomePage(WebDriver driver) {
        super(driver);
    }
}
