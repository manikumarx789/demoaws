package com.selenium.test.pages;

import com.selenium.framework.base.SeleniumBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("cucumber-glue")
public class BookFlightTicket extends SeleniumBase {
    @FindBy(xpath = "/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[2]/td[2]/b/font/input[2]")
    public static WebElement ONEWAY_RADIOBUTTON;
    @FindBy(xpath = "/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[4]/td[2]/select/option[3]")
    public static WebElement SELECTORIGIN_DROPDOWN;
    @FindBy(xpath = "/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[5]/td[2]/select[1]/option[6]")
    public static WebElement SELECTDATE_DROPDOWN;
    @FindBy(xpath = "/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[9]/td[2]/font/font/input[1]")
    public static WebElement SELECTCLASS_RADIOBUTTON;
    @FindBy(xpath = "/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[10]/td[2]/select/option[3]")
    public static WebElement SELECTAIRLINE_DROPDOWN;
    @FindBy(xpath = "/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[14]/td/input")
    public static WebElement BOOKTICKETCONTINUE_BUTTON;
    @FindBy(xpath = "/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[14]/td/input")
    public static WebElement BOOKTICKETCONTINUEDEPART_BUTTON;
    @FindBy(name = "passFirst0")
    public static WebElement PASSENGERFIRSTNAME;
    @FindBy(name = "passLast0")
    public static WebElement PASSENGERLASTNAME;
    @FindBy(name = "pass.0.meal")
    public static WebElement SELECTMEALS_DROPDOWN;
    @FindBy(name = "creditnumber")
    public static WebElement CREDITCARDNUMBER;
    @FindBy(xpath ="/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[23]/td/input")
    public static WebElement BOOKTICKETPURCHASE_BUTTON;
    @FindBy(xpath ="/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr[1]/td[2]/table/tbody/tr[1]/td/img/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr[1]/td[2]/table/tbody/tr[1]/td/img")
    public static WebElement FLIGHTBOOKINGCONFIRMAITON;
    
    public BookFlightTicket(WebDriver driver) {
        super(driver);
    }
}
