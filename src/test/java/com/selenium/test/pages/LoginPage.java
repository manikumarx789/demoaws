package com.selenium.test.pages;

import com.selenium.framework.base.SeleniumBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("cucumber-glue")
public class LoginPage extends SeleniumBase {
    @FindBy(xpath = "/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td[1]/a")
    public static WebElement LOGIN_BUTTON;
    @FindBy(name = "userName")
    public static WebElement LOGINUSERNAME;
    @FindBy(name = "password")
    public static WebElement LOGINPASSWORD;
    @FindBy(xpath = "/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[4]/td/input")
    public static WebElement SUBMITLOGIN_BUTTON;
    @FindBy(xpath = "/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td/img")
    public static WebElement FLIGHTFINDER;
    
    public LoginPage(WebDriver driver) {
        super(driver);
    }
}
