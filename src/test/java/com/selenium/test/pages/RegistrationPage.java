package com.selenium.test.pages;

import com.selenium.framework.base.SeleniumBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("cucumber-glue")
public class RegistrationPage extends SeleniumBase {
    @FindBy(xpath = "/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/a")
    public static WebElement REGISTER_BUTTON;
    @FindBy(name = "firstName")
    public static WebElement FIRSTNAME;
    @FindBy(name = "lastName")
    public static WebElement LASTNAME;
    @FindBy(name = "phone")
    public static WebElement PHONENUMBER;
    @FindBy(name = "userName")
    public static WebElement EMAIL;
    @FindBy(name = "address1")
    public static WebElement ADDRESS;
    @FindBy(name = "city")
    public static WebElement CITY;
    @FindBy(name = "state")
    public static WebElement STATE;
    @FindBy(name = "email")
    public static WebElement USERNAME;
    @FindBy(name = "password")
    public static WebElement PASSWORD;
    @FindBy(name = "confirmPassword")
    public static WebElement CONFIRMPASSWORD;
    @FindBy(xpath ="/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[18]/td/input" )
    public static WebElement SUBMIT_BUTTON;
    @FindBy(xpath = "/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td/p[2]/font/a[1]")
    public static WebElement REGISTRATION_VALIDATION; 
    public RegistrationPage(WebDriver driver) {
        super(driver);
    }
}
