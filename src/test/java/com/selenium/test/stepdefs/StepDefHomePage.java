package com.selenium.test.stepdefs;

import com.selenium.test.pages.HomePage;
import com.selenium.test.pages.Navigations;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;


import static com.selenium.test.pages.HomePage.*;


public class StepDefHomePage {

    @Autowired
    
    public HomePage homepage;
    
    @Given("^I am on 'Home Page' screen$")
    public void givenIAmOnHomePageScreen() {
    	homepage.getURL("");
    	homepage.assertElementPresent(HOMEPAGETITLE,10);
    }

    @Then("^I return to  'Home Page' screen$")
    public void thenHomepageshouldbedisplayed() {
       	homepage.assertElementPresent(HOMEPAGETITLE,10);
    }
}
