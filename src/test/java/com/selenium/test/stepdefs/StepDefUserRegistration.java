package com.selenium.test.stepdefs;

import com.selenium.test.pages.RegistrationPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import java.util.concurrent.ThreadLocalRandom;
import org.springframework.beans.factory.annotation.Autowired;


import static com.selenium.test.pages.RegistrationPage.*;


public class StepDefUserRegistration {

    @Autowired

    public RegistrationPage registrationpage;
    
    @When("^Click user Registration$")
    public void whenuserRegistrationPageshouldbedisplayed() {
    	registrationpage.assertElementPresent(REGISTER_BUTTON,10);
    	REGISTER_BUTTON.click();
    }
    
    @And("^Enter User Details$")
    public void WhenIEnterUserDetailsforRegistration() {
    	registrationpage.assertElementPresent(REGISTER_BUTTON,10);
    	FIRSTNAME.sendKeys("TESTFIRST");
    	LASTNAME.sendKeys("TESTLAST");
    	PHONENUMBER.sendKeys("9999999999");
    	EMAIL.sendKeys("123test@gmail.com");
    	ADDRESS.sendKeys("ABC STREET");
    	CITY.sendKeys("ALBANY");
    	STATE.sendKeys("NEW YORK");
    	int RandomNbrusr = generateRandomIntIntRange(1234,5678);
    	USERNAME.sendKeys("TEST CODE" + RandomNbrusr);
    	int RandomNbrpass = generateRandomIntIntRange(1234,5678);
    	PASSWORD.sendKeys("TEST123" + RandomNbrpass);
    	CONFIRMPASSWORD.sendKeys("TEST123" + RandomNbrpass);
    	SUBMIT_BUTTON.click();  	
    }
    
    @Then("^Valdiate if Registration is Successful$")
    public void thenValidateRegistrationsuccessful() {
    	registrationpage.assertElementPresent(REGISTRATION_VALIDATION,10);
    }

public static int generateRandomIntIntRange(int min,int max) {
	int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
    System.out.println("Random Numnber : " +randomNum);
    return randomNum;
}
}
