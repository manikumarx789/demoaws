package com.selenium.test.stepdefs;

import com.selenium.test.pages.Navigations;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;


import static com.selenium.test.pages.Navigations.*;


public class StepDefScreenNavigations {

    @Autowired
    
    public Navigations navigations;
    
    @When("^I click on support,valdiate page and click on Back to Home$")
    public void thensupportpageshouldbedisplayed() {
    	SUPPORT_BUTTON.click();
    	navigations.assertElementPresent(NAVIGATIONTITLE,10);
    	navigations.assertElementPresent(NAVIGATION_BACKHOMEBUTTON,10);
    	NAVIGATION_BACKHOMEBUTTON.click();
    }

    @When("^I click on Contact,validate page and click on Back to Home$")
    public void thencontactpageshouldbedisplayed() {
    	CONTACT_BUTTON.click();
    	navigations.assertElementPresent(NAVIGATIONTITLE,10);
    	navigations.assertElementPresent(NAVIGATION_BACKHOMEBUTTON,10);
    	NAVIGATION_BACKHOMEBUTTON.click();
    }
}
