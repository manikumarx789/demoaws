package com.selenium.test.stepdefs;

import com.selenium.test.pages.BookFlightTicket;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;


import static com.selenium.test.pages.BookFlightTicket.*;


public class StepDefBookFlightTickets {

    @Autowired

    public BookFlightTicket bookflightticket;
    
    @Then("^I am on Flight Finder page screen$")
    public void whenUserisOnFlightBookingScreenEnterDetails() {
    	bookflightticket.assertElementPresent(ONEWAY_RADIOBUTTON,10);
    	ONEWAY_RADIOBUTTON.click();
    	bookflightticket.assertElementPresent(SELECTORIGIN_DROPDOWN,10);
    	SELECTORIGIN_DROPDOWN.click();
    	bookflightticket.assertElementPresent(SELECTDATE_DROPDOWN,10);
    	SELECTORIGIN_DROPDOWN.click();
    	SELECTCLASS_RADIOBUTTON.click();
    	bookflightticket.assertElementPresent(SELECTORIGIN_DROPDOWN,10);
    	SELECTAIRLINE_DROPDOWN.click();
    	BOOKTICKETCONTINUEDEPART_BUTTON.click();
    	PASSENGERFIRSTNAME.sendKeys("FIRSTNAME");
    	PASSENGERLASTNAME.sendKeys("LASTNAME");
    	SELECTMEALS_DROPDOWN.click();
    	CREDITCARDNUMBER.sendKeys("1234567890");
    	BOOKTICKETPURCHASE_BUTTON.click();
    }
    
    @And("^Valdiate Booking Confirmation$")
    public void validateFlightBookingConfitmation() {
    	bookflightticket.assertElementPresent(FLIGHTBOOKINGCONFIRMAITON,10);
    }
}
