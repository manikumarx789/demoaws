package com.selenium.test.stepdefs;

import com.selenium.test.pages.LoginPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;


import static com.selenium.test.pages.LoginPage.*;


public class StepDefUserLogin {

    @Autowired

    public LoginPage loginpage;
    
    @When("^Click Login Button$")
    public void whenuserclicksloginbuttonloginPageshouldbedisplayed() {
    	loginpage.assertElementPresent(LOGIN_BUTTON,10);
    	LOGIN_BUTTON.click();
    }
    
    @And("^Enter '(.*)' as userName and '(.*)' as Password$")
    public void WhenIEnterUserIDandPassword(String username, String password) {
    	loginpage.assertElementPresent(LOGINUSERNAME,10);
    	LOGINUSERNAME.sendKeys(username);
    	LOGINPASSWORD.sendKeys(password);
    	SUBMITLOGIN_BUTTON.click();

    }
    
    @Then("^Validate Login Successful$")
    public void thenValidateLoginsuccessful() {
    	loginpage.assertElementPresent(FLIGHTFINDER,10);
    }
    
    @Then("^User should not login and portal should be on same screen$")
    public void thenValidateLoginUnsuccessful() {
    	loginpage.assertElementPresent(LOGINUSERNAME,10);
    }
}
