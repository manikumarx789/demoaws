
Feature: Valdiate Website for user Registration, Login and Navigations


Scenario: Validate support section in WebPage
        Given I am on 'Home Page' screen

        When I click on support,valdiate page and click on Back to Home
        Then I return to  'Home Page' screen 
   
        When I click on Contact,validate page and click on Back to Home
        Then I return to  'Home Page' screen
   
Scenario: User  should be able to register in Web portal
        Given I am on 'Home Page' screen
        When Click user Registration
        And Enter User Details
        Then Valdiate if Registration is Successful
 
 Scenario: Login with valid USERID in Portal  
		 		Given I am on 'Home Page' screen
		 		When Click Login Button
		 		And Enter 'TEST CODE' as userName and 'TEST123' as Password 
		 		Then  Validate Login Successful
 		
 Scenario: User should not able to login with Invalid userid and password  
		 		Given I am on 'Home Page' screen
		 		When Click Login Button
		 		And Enter 'TEST CODE1' as userName and 'TEST123' as Password 
		 		Then  User should not login and portal should be on same screen
 		
 #Scenario: User is able to book ticket after login
	 #    Given I am on 'Home Page' screen
	 #		When Click Login Button
	 #		And Enter 'TEST CODE' as userName and 'TEST123' as Password 
	 #    And  Validate Login Successful
	 #    Then I am on Flight Finder page screen
	 #    And Valdiate Booking Confirmation
    